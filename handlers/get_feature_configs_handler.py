from microservice_system_engine.utils import FeatureData, FeatureVersion
from utils.neo4j_util import Neo4jConnection
from typing import List


class GetFeatureConfigsFeatureData:
    def __init__(self, transformer_name: str, feature_name: str, implementation: int):
        self.transformer_name = transformer_name
        self.feature_name = feature_name
        self.implementation = int(implementation)


class GetFeatureConfigsHandlerData:
    def __init__(self, feature_data: List[GetFeatureConfigsFeatureData], project: int):
        self.feature_data = feature_data
        self.project = project

    @staticmethod
    def create_instance(raw) -> 'GetFeatureConfigsHandlerData':
        feature_data = raw.args.getlist('feature_data') or []
        features_data = [GetFeatureConfigsFeatureData(
            part['transformer_name'],
            part['feature_name'],
            part['implementation']
        ) for part in feature_data]

        return GetFeatureConfigsHandlerData(
            features_data,
            raw['project'],
        )


class GetFeatureConfigsResponseData:
    def __init__(self, config: FeatureData):
        self.config = config


class GetFeatureConfigsHandler:
    @staticmethod
    def __parse_body(body) -> GetFeatureConfigsHandlerData:
        return GetFeatureConfigsHandlerData.create_instance(body)

    @staticmethod
    def __generate_query(body: GetFeatureConfigsHandlerData):
        query = 'MATCH (t:Transformer)-[:transformer_has_feature]->(f:Feature)-[:feature_realization]->(fi:FeatureImplementation) WHERE '
        for body_part in body.feature_data:
            query += '(t.name="' + body_part.transformer_name + '", f.name="' + body_part.feature_name + '", fi.implementation=' + str(
                body_part.implementation) + ') OR '
        query = query[:-3]
        query += 'MATCH (fi)-[:feature_version]->(fv:FeatureVersion) WHERE fv.project=' + str(body.project)
        query += 'RETURN t.name, f.name, fv.major_version, fv.minor_version, fv.minor_minor_version, fi.implementation, fv.project'
        return query

    @staticmethod
    def handle(connection: Neo4jConnection, raw_body):
        body = GetFeatureConfigsHandler.__parse_body(raw_body)
        query = GetFeatureConfigsHandler.__generate_query(body)
        result = connection.query(query)
        return [GetFeatureConfigsResponseData(FeatureData(
            r[0],
            r[1],
            FeatureVersion(r[2], r[3], r[4], r[5], r[6])
        )) for r in result]
