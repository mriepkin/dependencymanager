from microservice_system_engine.utils import FeatureData, FeatureVersion
from utils.neo4j_util import Neo4jConnection
from typing import List
from microservice_system_engine.messaging import MessageManager
from microservice_system_engine.messaging import CreateFeatureRequest
from microservice_system_engine.utils import FeatureDataCache


class CreateFeatureData:
    def __init__(self, feature_data: FeatureData, destination: str, dependencies: List[FeatureData]):
        self.feature_data = feature_data
        self.destination = destination
        self.dependencies = dependencies


    @staticmethod
    def create_instance(raw) -> 'CreateFeatureData':
        return CreateFeatureData(
            FeatureData.from_dict(raw['feature_data']),
            raw['destination'],
            [FeatureData.from_dict(dependency) for dependency in raw['dependencies']]
        )


class CreateFeatureHandler:
    @staticmethod
    def __parse_body(body) -> CreateFeatureData:
        return CreateFeatureData.create_instance(body)

    @staticmethod
    def _get_dependencies(dependencies: List[FeatureData]):
        query = 'MATCH (t:Transformer)-[:transformer_has_feature]->(f:Feature)-[:feature_description]->(fd:FeatureDescription) WHERE '
        for dependency in dependencies:
            query += '(t.name="' + dependency.transformer_name
            query += '" AND f.name="' + dependency.feature_name
            query += '" AND fd.project=' + str(dependency.feature_version.project_code.value)
            query += ' AND fd.major_version=' + str(dependency.feature_version.major_version)
            query += ' AND fd.minor_version=' + str(dependency.feature_version.minor_version)
            query += ' AND fd.minor_minor_version=' + str(dependency.feature_version.minor_minor_version)
            query += ') OR '
        if len(dependencies) > 0:
            query = query[:-3]
        else:
            query = query[:-6]
        return query

    @staticmethod
    def _check_same_feature(connection, feature_data: FeatureData):
        query = CreateFeatureHandler._get_dependencies([feature_data])
        query += 'RETURN COUNT(fd)'
        result = connection.query(query)
        return result[0][0]

    @staticmethod
    def __generate_query(body: CreateFeatureData):
        query = ''
        if len(body.dependencies) > 0:
            query = CreateFeatureHandler._get_dependencies(body.dependencies)
        query += 'MERGE (other_t:Transformer) SET other_t.name="' + body.feature_data.transformer_name + '" SET other_t.destination="' + body.destination + '" '
        query += 'MERGE (other_f:Feature) SET other_f.name="' + body.feature_data.feature_name + '" '
        query += 'MERGE (other_fd:FeatureDescription) SET other_fd.major_version={}, other_fd.minor_version={}, other_fd.minor_minor_version={}, other_fd.implementation={}, other_fd.project={}, other_fd.name="{}" '\
            .format(
            body.feature_data.feature_version.major_version,
            body.feature_data.feature_version.minor_version,
            body.feature_data.feature_version.minor_minor_version,
            body.feature_data.feature_version.implementation,
            body.feature_data.feature_version.project_code.value,
            body.feature_data.feature_name
        )
        query += 'MERGE (other_t)-[:transformer_has_feature]->(other_f) '
        query += 'MERGE (other_f)-[:feature_description]->(other_fd) '
        if len(body.dependencies) > 0:
            query += 'MERGE (other_fd)-[:feature_depend_on]->(fd) '
        query += 'RETURN other_fd.name, other_fd.major_version, other_fd.minor_version, other_fd.minor_minor_version'
        return query

    @staticmethod
    def handle(connection: Neo4jConnection, raw_body, message_manager: MessageManager):
        body = CreateFeatureHandler.__parse_body(raw_body)
        dependency_query = CreateFeatureHandler._get_dependencies(body.dependencies) + ' RETURN COUNT(fd)'
        dependency_count = connection.query(dependency_query)[0][0]
        if dependency_count != len(body.dependencies):
            return 'Something wrong with dependencies number, specified {}, found {}'.format(len(body.dependencies), dependency_count)
        if CreateFeatureHandler._check_same_feature(connection, body.feature_data) > 0:
            return 'Such feature already exists!'
        query = CreateFeatureHandler.__generate_query(body)
        res = connection.query(query)
        message_manager.send_message('coordinator', CreateFeatureRequest(FeatureDataCache(body.feature_data, body.destination, [d.to_string() for d in body.dependencies])))
        message_manager.flush_messages()
        return 'ok'
