from flask import Flask
from flask import request
from utils.neo4j_util import Neo4jConnection
from handlers.get_feature_configs_handler import GetFeatureConfigsHandler
from handlers.create_feature_handler import CreateFeatureHandler

# messaging system
from microservice_system_engine.configs import MessageSystemConfig
from microservice_system_engine.messaging import MessageSystem
from microservice_system_engine.messaging import MessageManager
from microservice_system_engine.loggers import LoggerManager

import json


app = Flask(__name__)
conn = Neo4jConnection(uri="bolt://10.11.0.66:7687", user="neo4j", pwd="parlor-museum-arthur-spell-guitar-8303")


def __read_json(path: str):
    with open(path, 'r') as f:
        return json.loads(''.join(f.readlines()))


def init_message_system(path_to_config: str):
    json_config = __read_json(path_to_config)
    config = MessageSystemConfig(MessageSystemConfig.Producer(json_config['producer']['server'], lambda x: json.dumps(x).encode('utf-8')),
                                 MessageSystemConfig.Consumer(json_config['consumer']['topic'], json_config['consumer']['server'], lambda x: json.loads(x.decode('utf-8')), auto_offset_reset=json_config['consumer']['auto_offset_reset']))
    return MessageSystem(config)


message_system = init_message_system('./configs/messaging_config.json')
message_manager = MessageManager(LoggerManager, message_system)

@app.route('/get-feature-dependencies', methods=['GET'])
def get_feature_dependencies():
    print('qwe')


@app.route('/create-feature', methods=['POST'])
def create_feature():
    return CreateFeatureHandler.handle(conn, request.get_json(), message_manager)


@app.route('/get-feature-configs', methods=['GET'])
def get_feature_configs():
    return GetFeatureConfigsHandler.handle(conn, request)

if __name__ == '__main__':
    app.run(use_reloader=True)
